#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import json
import scipy
from scipy.optimize import fmin_cg

def get_orientation(x,alpha1,alpha2,d1,d2,l):

	# alpha = np.arccos((-l**2+d2**2+d1**2)/(2*d1*d2)) #radians
	beta = np.arcsin(x/d1) #radians
	gamma = np.arcsin(x/d2) #radians


	# alpha_new = alpha2-alpha1
    #
    #
    # if alpha1<0 and alpha2>0:
		# theta = np.pi-beta-alpha+alpha2
	theta = gamma + alpha2
	if alpha2 < 0:
		theta = beta + alpha1
	# elif alpha1>0 and alpha2>0:
	# 	theta = alpha2 + gamma


	# print "alpha",alpha,alpha_new
	# print "angle",alpha+beta+gamma

	return theta

def get_position_geometric(alpha1,alpha2,d1,d2,l):
	alpha = np.arccos((-l**2+d2**2+d1**2)/(2*d1*d2)) #radians
	h = (d1*d2*np.sin(alpha))/l

	x = h
	y = np.sqrt(d1**2-h**2)

	theta = get_orientation(x,alpha1,alpha2,d1,d2,l)

	return np.array([x,y,theta])

def A(x,y,phi):
	return np.array([[np.cos(phi),-np.sin(phi),x],
					[np.sin(phi),np.cos(phi),y],
					[0,0,1]]
					)

def print_scan(theta,scan,theta1,corners):
	fig1 = plt.figure()
	ax1 = fig1.add_axes([0.1,0.1,0.8,0.8],polar=True)
	ax1.plot(theta,scan)
	ax1.plot(theta1,corners,'ro')
	#ax1.set_ylim(0,2)  # distance range
	plt.title(name+"_corner_detection")
	plt.show()

def get_corners(theta,scan):
	corners = np.array(scan)

	corners1 = []
	theta1 = []

	for i in range(len(corners)-1):
		if np.abs(np.abs(corners[i]) - np.abs( corners[i+1])) >= 0.1:
			corners1.append(corners[i])
			corners1.append(corners[i+1])
			theta1.append(theta[i])
			theta1.append(theta[i+1])

	return theta1,corners1

def filtr_scan(scan):
	scan = np.array(scan)
	x = np.arange(0,len(scan))
	theta = np.array((np.pi/len(scan) )*x)  # angle in rad

	scan1 = []
	theta1 = []
	for i in range(len(scan)):
		if not (np.isinf(scan[i])):
			if not np.isnan(scan[i]):
				scan1.append(scan[i])
				theta1.append(theta[i])

	return np.array(theta1),np.array(scan1)

def pose_transformed(pose):
	theta = pose[2]*np.pi/180
	phi = 1.6
	pose1 = np.dot(A(1 ,1 ,phi),[pose[0],pose[1],1])
	return np.array([pose1[0],pose1[1],theta+phi])

def get_position_bilateration(alpha1,alpha2,d1,d2,l):
	x0 = [0,1]
	res=scipy.optimize.minimize(f, x0,args=(d1,d2,l),method='Nelder-Mead')

	x = np.abs(res.x[1])
	y = np.abs(res.x[0])

	theta = get_orientation(x,alpha1,alpha2,d1,d2,l)

	return np.array([x,y,theta])

def f(z, *args):
	d1, d2, xA2 = args

	x=z[0]
	y=z[1]

	return np.abs(d1-np.sqrt((x)**2+(y)**2))+np.abs(d2-np.sqrt((x-xA2)**2+(y)**2))

if __name__ == "__main__":


	# name = ['data2_stereo_fwd']
	name = ['data2_stereo_fwd','data2_stereo_rot']

	for j in range(len(name)):
		json_data = open(name[j]+'.json')
		data = json.load(json_data)


		print name[j]

		for i in range(len(data)):
		# for i in range(1):
			pose = data[i]['pose']
			alpha1 = data[i]['alpha1'] #radians
			alpha2 = data[i]['alpha2'] #radians
			d1 = data[i]['d1']
			d2 = data[i]['d2']
			time = data[i]['time']
			scan = data[i]['scan']

			theta, scan = filtr_scan(scan)
			theta_corners, corners = get_corners(theta,scan)
	#		print_scan(theta,scan,theta_corners,corners)


			# print theta_corners
			# print alpha1, alpha2
			# print "  ",i,"d1,d2,a1,a2 ",np.array([d1,d2]),alpha1,alpha2
			print "                  [m            , m         , radians ]"
			print "  ",i,"geometric   ",get_position_geometric(alpha1,alpha2,d1,d2,0.57)
			print "  ",i,"bilateration",get_position_bilateration(alpha1,alpha2,d1,d2,0.57)
			print "  ",i,"transformed ",pose_transformed(pose)
			print "  ",i,"file        ",np.array([pose[0],pose[1],pose[2]*np.pi/180])
			# print "  ",i,"file        ",pose
			print






