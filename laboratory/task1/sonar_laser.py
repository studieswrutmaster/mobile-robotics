#!/usr/local/bin/python
# -*- coding: utf-8 -*-
# Pioneer robot sonar visualization

import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from time import sleep
from drive import RosAriaDriver

# TODO:
# Enter robot number as a ROS topic node prefix
robot=RosAriaDriver('/PIONIER5')

loop_count=25  # drawing iterations
beam_half_angle=7.5 # haf of sonar angular beam width

# 
# A function to calculate Cartesian coordinates to polar
#  result: a tuple (rho,phi)
#  rho - radius, phi - angle in degrees
def cart2pol(x, y):
    #TODO: calculations
    rho = np.sqrt(x**2+y**2)
    phi = np.arctan2(y,x)
    return(phi, rho)

# A function to transform polar  coordinates to Cartesian
# input angle in degrees
# returns a tuple (x,y)
def pol2cart(rho, phi):
    #TODO: calculations
    x = rho*np.cos(phi)
    y = rho*np.sin(phi)
    return(x, y)

# plotting data
def plotsonars(ax,sonarreads):
    pol=[cart2pol(x[0],x[1]) for x in sonarreads ]
    for item in pol:
        #print item[0],item[1]
        wedge = mpatches.Wedge([0,0], item[0], item[1]-beam_half_angle, item[1]+beam_half_angle, alpha=0.4, ec="black",fc="CornflowerBlue")
        ax.add_patch(wedge)

def plotarrows(ax,arrlist):
    y=[[0,0]+x for x in arrlist ]
    print y
    soa =np.array(y) 
    X,Y,U,V = zip(*soa)
    ax.quiver(X,Y,U,V,angles='xy',scale_units='xy',scale=1)


fig = plt.figure()
#ax = plt.gca()
#ax.set_aspect('equal')
#ax.set_xlim([-6,6])
#ax.set_ylim([-6,6])



x = np.arange(0,512)
theta = (np.pi/512 )*x  # angle in rad 
#print theta

ax = fig.add_axes([0.1,0.1,0.8,0.8],polar=True)
ax.set_ylim(0,2)  # distance range

plt.ion()
plt.show()

for i in range(loop_count):
    sonar=robot.ReadSonar()
    laser=robot.ReadLaser()
    ax.cla()
    ax.plot(theta-np.pi/2,laser,lw=2.5)

    for j in range(len(sonar)):
	phi,rho = cart2pol(sonar[j][0],sonar[j][1])
	#phi = np.degrees(phi)
	print phi,rho
	ax.plot(phi,rho,'ro',lw=2.5)


#    plotsonars(ax,skan)  
#    plotarrows(ax,skan)
#    ax.set_xlim([-6,6])
#    ax.set_ylim([-6,6])
    plt.draw()
    plt.pause(0.0001)
    plt.savefig('foo'+str(i)+'.png')
    sleep(0.1)


