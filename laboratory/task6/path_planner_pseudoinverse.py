#!/usr/bin/env python

#/*****************************************************************************
# *                                                                           *
# *   path_planner_pseudoinverse.py                                             *
# *                                                                           *
# *   ROS                                                                     *
# *                                                                           *
# *   Copyright (C) 2015 by Lukasz Chojnacki                                  *
# *   lukasz.chojnacki@pwr.edu.pl                                               *
# *                                                                           *
# *   This program is free software; you can redistribute it and/or modify    *
# *   it under the terms of the GNU General Public License as published by    *
# *   the Free Software Foundation; either version 2 of the License, or       *
# *   (at your option) any later version.                                     *
# *                                                                           *
# *   This program is distributed in the hope that it will be useful,         *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
# *   GNU General Public License for more details.                            *
# *                                                                           *
# *   You should have received a copy of the GNU General Public License       *
# *   along with this program; if not, write to the                           *
# *   Free Software Foundation, Inc.,                                         *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
# *                                                                           *
# *****************************************************************************/

from sys import path
path.append(r"/mnt/lab/lchojnac/catkin_ws/src/motion_planning_and_tracking_pkg/src/ECSA/casadi-py27-np1.9.1-v3.0.0")
from casadi import *

import rospy
import matplotlib.pyplot as plt

from ECSA.Control import Fourier
from ECSA.System_base import Monocycle
from ECSA.Algorithm import Pseudoinverse

from std_msgs.msg import Float64MultiArray, Bool
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry

import tf

ackFlag = True


#-------------------------------------------#
# Algorithm parameters

# derivative time
T = int(rospy.get_param('/robrex/algorithm/T',20.0))#s
rospy.set_param('/robrex/algorithm/T',T)
#T = T * 1000#ms

# algoritm step
gamma = float(rospy.get_param('/robrex/algorithm/gamma',0.94))
rospy.set_param('/robrex/algorithm/gamma',gamma)

# total tolerance error
TOL_tmp = rospy.get_param('/robrex/algorithm/TOL',[1,10,-1])
TOL = 10**(-3)#float(TOL_tmp[0])*pow(float(TOL_tmp[1]),float(TOL_tmp[2]))
rospy.set_param('/robrex/algorithm/TOL',TOL)

# maximum number of steps
k_max = int(rospy.get_param('/robrex/algorithm/k_max',300) )
rospy.set_param('/robrex/algorithm/k_max',k_max)

# 
dts = rospy.get_param('/robrex/controller/dts',0.04)#s
rospy.set_param('/robrex/controller/dts',dts)

#
Ts = rospy.get_param('/robrex/controller/Ts',10)#s
rospy.set_param('/robrex/controller/Ts',Ts)

d_notify = int(rospy.get_param('/robrex/controller/d_notify',1))
rospy.set_param('/robrex/controller/d_notify',d_notify)

step = Ts/dts

notify = step - d_notify

#-------------------------------------------#
opts = {}
opts['T'] = T
opts['gamma0'] = gamma
opts['TOL'] = TOL
opts['k_max'] = k_max


la1 = [0.5, 0.25, -0.25, 0.1, 0.1]
la2 = [-0.5, 0.25, 0.25, 0.1, 0.1, 0.01, 0.01]
opts['lambda0'] = vertcat([0.5, 0.25, -0.25, 0.1, 0.1],[ -0.5, 0.25, 0.25, 0.1, 0.1, 0.01, 0.01])
opts['lambda'] = [[len(la1),0],[len(la2),0]]

opts['viz'] = [False ,False,0.2]


fourier = Fourier(opts)
system = Monocycle(fourier)

def Algorithm(Yd):
    global robot_position
    ## translate point list

    opts['q0'] = robot_position
    opts['x0'] = []
    opts['yd'] = Yd

    # create algorithm object
    algorithm = Pseudoinverse(system,opts)

    # run algorithm
    out = algorithm.run()
    rospy.loginfo("Planning finished")
    dt = T/float(len(out['tgrid']))
    
    return dt,np.array(out['q'][0,:]),np.array(out['q'][1,:]),np.array(out['u'][0,:]),np.array(out['u'][1,:])

def DesiredPlan(data):
    global ackFlag
    global readyFlag
    global q1,q2,Yd
    global m


    if ackFlag == True:
        ackFlag = False
        m = 0

        # read data from topic /desired_plan
        # build a point map form a recived vector

        Yd = data.data
        print "Yd=",Yd

        ackFlag = not ackFlag

        # generate trajectory
        dt,q1,q2,u1,u2 = Algorithm(Yd)

        control_msg = Twist()
        r = 1.0/dt
        print dt
        rate = rospy.Rate(50) 
        for i in range(0,len(u1[0])):
            control_msg.linear.x = u1[0][i]
            control_msg.angular.z = u2[0][i]
            rate.sleep()
            pubTraj.publish(control_msg)
        
        ackFlag = True
        rospy.loginfo("Sending control values done. ")        

    else:
        rospy.loginfo("Sorry, I'm working. ack flag is false. ")

def Update_pose(data):
    global robot_position
    quaternion = (
        data.pose.pose.orientation.x,
        data.pose.pose.orientation.y,
        data.pose.pose.orientation.z,
        data.pose.pose.orientation.w)
    euler = tf.transformations.euler_from_quaternion(quaternion)
    robot_position = [data.pose.pose.position.x, data.pose.pose.position.y, euler[2]]


def node():
    global pubAck
    global pubTraj
    global robot_position
    global q1,q2,Yd
    global ackFlag

    robot_position = [0,0,0]
    Yd = [0,0,0]    
    q1 = np.array([])
    q2 = np.array([])
    # Init ROS Node /LiftedNewtonAlgorithm
    rospy.init_node('path_planner_pseudoinverse')

    # Publishers
    pubAck = rospy.Publisher("ack", Bool, queue_size=100)
    pubTraj = rospy.Publisher("cmd_vel", Twist, queue_size=100)
    
    # Subscribers
    rospy.Subscriber("desired_plan", Float64MultiArray, DesiredPlan)
    rospy.Subscriber("pose", Odometry, Update_pose)

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        plt.ion()
        plt.plot(robot_position[0],robot_position[1],'ok',
                Yd[0],Yd[1],'xk',q1.T,q2.T,'k')
        plt.title("robot position "+str(robot_position))
        if ackFlag == False:
            plt.clf()
        plt.pause(0.001)

        rate.sleep()
        #rospy.spinOnce()    
    #print T,gamma,TOL,k_max,dts,Ts
    
    rospy.spin()


if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass






