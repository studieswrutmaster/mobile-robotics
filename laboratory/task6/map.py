#!/usr/bin/env python
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import json
import time
from drive import RosAriaDriver
import signal
<<<<<<< HEAD


=======
import rospy
import message_filters
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Quaternion
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float64MultiArray
import tf
import math


g_laser=None
g_pose=None
fig1= plt.figure(3)
ix=iy=0.0
rx=ry=0.0
abs_x=abs_y=0.0
#coords=[]
pub = rospy.Publisher('desired_plan',Float64MultiArray)

>>>>>>> d0073d0c0307b4a828ca73a44cb03f84a51671ab
def signal_handler(signal, frame):
    global interrupted
    interrupted = True

def filtr_scan(scan):
	scan = np.array(scan)
	x = np.arange(0,len(scan))
	theta = np.array((np.pi/len(scan) )*x)  # angle in rad

	scan1 = []
	theta1 = []
	for i in range(len(scan)):
		if not (np.isinf(scan[i])):
			if not np.isnan(scan[i]):
				scan1.append(scan[i])
				theta1.append(theta[i])

	return np.array(theta1),np.array(scan1)

def print_scan(name,theta,scan):
<<<<<<< HEAD
	fig1 = plt.figure(3)
	ax1 = fig1.add_axes([0.1,0.1,0.8,0.8],polar=True)
	ax1.plot(theta,scan)
	#ax1.set_ylim(0,2)  # distance range
	plt.title(name+"_map")
	#plt.show()
=======
    global fig1
    fig1 = plt.figure(3)
    ax1 = fig1.add_axes([0.1,0.1,0.8,0.8],polar=True)
    ax1.plot(theta,scan)
    #ax1.set_ylim(0,2)  # distance range
    plt.title(name+"_map")
    #plt.show()
>>>>>>> d0073d0c0307b4a828ca73a44cb03f84a51671ab


def make_map(square_size):#meters

	j = 2*10/square_size
	i = 2*8/square_size

	map = np.array([[0 for col in range(int(j))] for row in range(int(i))])

	return map

def show_map(map,pose,square_size):
	map_y,map_x = map.shape	
	plt.ion()

	map_tmp = make_map(square_size)
	
	for i in range(map_x):
		for j in range(map_y):
			map_tmp[j,i] = 1024*(1.-(1./(1.+float(np.exp(map[j,i])))))
<<<<<<< HEAD
			print map_tmp[j,i]
=======
			#print map_tmp[j,i]
>>>>>>> d0073d0c0307b4a828ca73a44cb03f84a51671ab

	#plt.figure(2)
	cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap',
                                           ['white','black'],
                                           1024)
	plt.imshow(map_tmp,interpolation='nearest',
                    cmap = cmap,origin='lower')
	pos_x,pos_y=xy2map(pose[0],pose[1],square_size)
	plt.plot(pos_x+map_x/2,pos_y+map_y/2,'xb')
<<<<<<< HEAD
=======
        global rx,ry
        rx=pos_x+map_x/2
        ry=pos_y+map_y/2
>>>>>>> d0073d0c0307b4a828ca73a44cb03f84a51671ab
	plt.pause(0.001)
	#plt.show(block=False)

def pol2cart(rho, phi):
    x = rho*np.cos(phi)
    y = rho*np.sin(phi)
    return(x, y)


def xy2map(x,y,square_size):
	return int(x/square_size),int(y/square_size)

def inverse_sensor_model(hit):
	if hit == True:
		return np.log10(0.9/0.1)
	else:
		return np.log10(0.4/0.6)

def update_map(map,pose,theta,scan,square_size):
	map_x,map_y = map.shape	
	
	for k in range(len(theta)):
		#print theta[k],scan[k]
		x,y = pol2cart(scan[k],theta[k]+pose[2]*np.pi/180)
		i,j = xy2map(x,y,square_size)
		#print i,j

		pos_x,pos_y=xy2map(pose[0],pose[1],square_size)
		#print i,j
		#print pose[0],pose[1]
		map[pos_y+map_x/2-i,pos_x+map_y/2+j] += 1.1*inverse_sensor_model(True)
	

	return map

def show_laser(theta,scan):

	x,y = pol2cart(scan,theta)

	plt.figure(1)
	plt.plot(x,y)
	#plt.show()

<<<<<<< HEAD

if __name__ == "__main__":

	signal.signal(signal.SIGINT, signal_handler)
	interrupted = False

	robot=RosAriaDriver('/PIONIER2')
	square_size = 0.05 #meters
	map = make_map(square_size)

	while True:
		x = np.arange(0,512)
		theta = ((np.pi/512 )*x)-np.pi/2  # angle in rad 
		pose = robot.GetPose()
		scan=robot.ReadLaser()
		theta, scan = filtr_scan(scan)
		map = update_map(map,pose,theta,scan,square_size)
		show_map(map,pose,square_size)
		
		if interrupted:
			break

=======
def update_pose(data):
    quaternion=(
        data.pose.pose.orientation.x,
        data.pose.pose.orientation.y,
        data.pose.pose.orientation.z,
        data.pose.pose.orientation.w)
    euler=tf.transformations.euler_from_quaternion(quaternion)
    global g_pose
    g_pose = [data.pose.pose.position.x,data.pose.pose.position.y,euler[2]*180/math.pi]
    
    
def callback_ps(pose_topic, laser_topic):


    # map = make_map(square_size)
    # square_size = 0.05 
    # x = np.arange(0,512)
    # theta = ((np.pi/512 )*x)-np.pi/2  # angle in rad 
    # #pose = robot.GetPose()
    # #scan=robot.ReadLaser()
    # theta, scan = filtr_scan(scan)
    # map = update_map(map,pose,theta,scan,square_size)
    # show_map(map,pose,square_size)
    print "dziala!!!"

def pose_callback(pose_topic):
    
    update_pose(pose_topic)
    

    
def laser_callback(laser_topic):
    global g_laser
    g_laser= laser_topic.ranges
    #print g_laser

def onclick(event):
    global ix, iy
    ix, iy = event.xdata, event.ydata
    print type(ix)
    abs_x=0.05*(ix-rx)
    abs_y=0.05*(iy-ry)
    print 'x = %d, y = %d, rx = %d, ry = %d, abs_x = %f, abs_y=%f'%(
        ix, iy,rx,ry, abs_x,abs_y)

    msg = Float64MultiArray()
    msg.data=[abs_x,abs_y,0]
    
    pub.publish(msg)



    #global coords
    #coords.append((ix, iy))

    #if len(coords) == 2:
    #    fig1.canvas.mpl_disconnect(cid)
    #return coords

def listener():
    
    rospy.init_node('map')

    rospy.Subscriber('pose_topic',Odometry,pose_callback)
    rospy.Subscriber('laser_topic',LaserScan,laser_callback)
    #pose_sub = message_filters.Subscriber('pose_topic', Odometry)
    #scan_sub = message_filters.Subscriber('laser_topic', LaserScan)

    print "list"
    #ts = message_filters.TimeSynchronizer([pose_sub, scan_sub], 100)
    #ts.registerCallback(callback_ps)

    signal.signal(signal.SIGINT, signal_handler)
    interrupted = False
    square_size = 0.05 #meters
    map = make_map(square_size)
    
    global g_pose, g_laser

    
    cid = fig1.canvas.mpl_connect('button_press_event', onclick)
    
    while True:
        time.sleep(0.5)
	x = np.arange(0,512)
	theta = ((np.pi/512 )*x)-np.pi/2  # angle in rad 
	pose = g_pose   
	scan= g_laser
	theta, scan = filtr_scan(scan)
        #print pose
	map = update_map(map,pose,theta,scan,square_size)
	show_map(map,pose,square_size)
	
	if interrupted:
	    break

    
    rospy.spin()

        

if __name__ == "__main__":
    print "dzi!!"
    listener()
    
    # signal.signal(signal.SIGINT, signal_handler)
    # interrupted = False
    # square_size = 0.05 #meters
    # map = make_map(square_size)
    
    # global g_pose, g_laser
    
    # while True:
    #     time.sleep(1)
    #     x = np.arange(0,512)
    #     theta = ((np.pi/512 )*x)-np.pi/2  # angle in rad 
    #     pose = g_pose   
    #     scan= g_laser
    #     theta, scan = filtr_scan(scan)
    #     map = update_map(map,pose,theta,scan,square_size)
    #     show_map(map,pose,square_size)
	
    #     if interrupted:
    #         break







    
    #map = make_map(square_size)
		# for i in range(len(data)):
    #for i in range(1):
#	pose = data[i]['pose']
#	time = data[i]['time']
#	scan = data[i]['scan']



#    signal.signal(signal.SIGINT, signal_handler)
#    interrupted = False
    
#    robot=RosAriaDriver('/PIONIER2')
    # square_size = 0.05 #meters
    # map = make_map(square_size)
    
    # while True:
    #     x = np.arange(0,512)
    #     theta = ((np.pi/512 )*x)-np.pi/2  # angle in rad 
    #     pose = robot.GetPose()
    #     scan=robot.ReadLaser()
    #     theta, scan = filtr_scan(scan)
    #     map = update_map(map,pose,theta,scan,square_size)
    #     show_map(map,pose,square_size)
	
    #     if interrupted:
    #         break
            
>>>>>>> d0073d0c0307b4a828ca73a44cb03f84a51671ab
#	name = ['map_boxes_0']
#
#	for j in range(len(name)):
#		json_data = open(name[j]+'.json')
#		data = json.load(json_data)
#
#
#		square_size = 0.1 #meters
#
#		map = make_map(square_size)
#		for i in range(len(data)):
#		#for i in range(1):
#			pose = data[i]['pose']
#			time1 = data[i]['time']
#			scan = data[i]['scan']
#
#
#			print pose
#			print time1
#			print 
#			theta, scan = filtr_scan(scan)
#			#print_scan(name[0],theta,scan)
#			#show_laser(theta,scan)
#			map = update_map(map,pose,theta,scan,square_size)
#			show_map(map,pose)
#			#time.sleep(1.0)
#			#print_scan(name[j],theta,scan)	
#			raw_input("Enter ")
