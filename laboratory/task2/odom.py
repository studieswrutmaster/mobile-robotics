#from drive import RosAriaDriver
import csv
from datetime import datetime
import numpy as np
from math import cos,sin,pi
import matplotlib.pyplot as plt

time = []
vl = []
vr = []
pl = []
pr = []

with open('square_right.csv') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';', quotechar='|')
    for row in reader:        
        time.append(float(datetime.strptime(row['#time'],"%H:%M:%S.%f").strftime("%S.%f")))
	vl.append(float(row['velL']))
	vr.append(float(row['velR']))
	pl.append(float(row['posL']))
	pr.append(float(row['posR']))

#robot=RosAriaDriver('/PIONIER2')


## ROBOT PARAMETERS
d = 323 #mm


x=0
y=0
theta=0
xx = 0
yy = 0
thetaa = 0
ta = datetime.now()

x_all = []
y_all = []
x_pos_all = []
y_pos_all = []

impulse_per_wheel = 19150
R = 195/2
sk = 2*pi*R
k = sk/impulse_per_wheel

k = 1.0/132.0

s = 0

#print pr
#print pl

for i in range(len(vl)):
	tb=datetime.now()
	#[(posL,posR),(vL,vR)]=robot.ReadEncoder()
	#dtime = (tb-ta).seconds
	vL = vl[i]
	vR = vr[i]

	#####################################################
	if i != 0:
		dtime = time[i]-time[i-1]
	
		dtheta = dtime*(vR-vL)/d
		theta=theta+dtheta
		x += dtime*cos(theta)*(vL+vR)/2
		y += dtime*sin(theta)*(vL+vR)/2
		ta=datetime.now()
		x_all.append(x)
		y_all.append(y)
	#####################################################
	if i != 0:
		if abs(pr[i]-pr[i-1]) > 32000:
			dsr = abs(pr[i]-pr[i-1])-65536	
		else:
			dsr = pr[i]-pr[i-1]
		if abs(pl[i]-pl[i-1]) > 32000:
			dsl = abs(pl[i]-pl[i-1])-65536
		else:
			dsl = pl[i]-pl[i-1]

		ds = k*(dsr+dsl)/2
		dthetaa = k*(dsr-dsl)/d
		#print i,ds,dsr,dsl
		thetaa += dthetaa

		xx += ds*cos(thetaa)
		yy += ds*sin(thetaa)

		x_pos_all.append(xx)
		y_pos_all.append(yy)	
	
	

#print theta,thetaa

plt.figure(1)
plt.plot(x_all,y_all)
plt.xlabel('x [m]')
plt.ylabel('y [m]')
plt.title('encoders velocity')
plt.axis('equal')


plt.figure(2)
plt.plot(x_pos_all,y_pos_all)
plt.xlabel('x [m]')
plt.title('encoders position')
plt.axis('equal')
plt.ylabel('y [m]')
plt.show()


