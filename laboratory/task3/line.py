#!/usr/bin/env python

import numpy as np
from ransac import *

def augment(xys):
	axy = np.ones((len(xys), 3))
	axy[:, :2] = xys
	return axy

def estimate(xys):
	axy = augment(xys[:2])
	return np.linalg.svd(axy)[-1][-1, :]

def is_inlier(coeffs, xy, threshold):
	return np.abs(coeffs.dot(augment([xy]).T)) < threshold

def pol2cart(rho, phi):
	x = rho*np.cos(phi)
	y = rho*np.sin(phi)
	return np.array([x, y])

if __name__ == '__main__':
	from matplotlib import pylab
	import json


	n = 100
	max_iterations = 100
	goal_inliers = n * 0.3

	name = 'wall'
	json_data = open(name+'.json')
	scan = json.load(json_data)

	scan = np.array(scan)
	x = np.arange(0,len(scan))
	theta = np.array((np.pi/len(scan) )*x)  # angle in rad

	scan1 = []
	theta1 = []
	for i in range(len(scan)):
		if not (np.isinf(scan[i])):
			if not np.isnan(scan[i]):
				scan1.append(scan[i])
				theta1.append(theta[i])

	scan = np.array(scan1)
	theta = np.array(theta1)

	xys = pol2cart(scan[0],theta[0])
	for i in range(1,len(scan)):
		xys = np.vstack((xys,pol2cart(scan[i],theta[i])))


	scan1 = []
	theta1 = []
	for i in range(len(xys)):
		if xys[i][0] > -2 and xys[i][0] < 2:
			scan1.append(np.array([xys[i][0]+2,xys[i][1]]))

	xys = np.array(scan1)

	pylab.scatter(xys.T[0], xys.T[1])

	# RANSAC
	m, b = run_ransac(xys,
					  estimate,
					  lambda x, y: is_inlier(x, y, 0.01),
					  goal_inliers,
					  max_iterations,
					  20
					  )
	a, b, c = m
	pylab.plot([0, 10], [-c/b, -(c+10*a)/b], 'r')
	pylab.axis([0, 5, 0, 5])
	pylab.grid()
	pylab.ylabel('y[m]')
	pylab.xlabel('x[m]')
	pylab.title(name+"_line_detection")
	pylab.show()




