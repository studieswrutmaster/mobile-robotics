#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import json

name = 'step'
json_data = open(name+'.json')
scan = json.load(json_data)

scan = np.array(scan)
x = np.arange(0,len(scan))
theta = np.array((np.pi/len(scan) )*x)  # angle in rad

scan1 = []
theta1 = []
for i in range(len(scan)):
	if not (np.isinf(scan[i])):
		if not np.isnan(scan[i]):
			scan1.append(scan[i])
			theta1.append(theta[i])

scan = np.array(scan1)
theta = np.array(theta1)


corners = np.array(scan)

corners1 = []
theta1 = []

for i in range(len(corners)-1):
	if np.abs(np.abs(corners[i]) - np.abs( corners[i+1])) >= 0.1:
		# tmp = corners[i]
		corners1.append(corners[i])
		corners1.append(corners[i+1])
		theta1.append(theta[i])
		theta1.append(theta[i+1])


fig1 = plt.figure()
ax1 = fig1.add_axes([0.1,0.1,0.8,0.8],polar=True)
ax1.plot(theta,scan,'b')
ax1.plot(theta1,corners1,'ro')
ax1.set_ylim(0,2)  # distance range
plt.title(name+"_corner_detection")
plt.show()





