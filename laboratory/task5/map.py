#!/usr/bin/env python
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import json
import time
from drive import RosAriaDriver
import signal


def signal_handler(signal, frame):
    global interrupted
    interrupted = True

def filtr_scan(scan):
	scan = np.array(scan)
	x = np.arange(0,len(scan))
	theta = np.array((np.pi/len(scan) )*x)  # angle in rad

	scan1 = []
	theta1 = []
	for i in range(len(scan)):
		if not (np.isinf(scan[i])):
			if not np.isnan(scan[i]):
				scan1.append(scan[i])
				theta1.append(theta[i])

	return np.array(theta1),np.array(scan1)

def print_scan(name,theta,scan):
	fig1 = plt.figure(3)
	ax1 = fig1.add_axes([0.1,0.1,0.8,0.8],polar=True)
	ax1.plot(theta,scan)
	#ax1.set_ylim(0,2)  # distance range
	plt.title(name+"_map")
	#plt.show()


def make_map(square_size):#meters

	j = 2*10/square_size
	i = 2*8/square_size

	map = np.array([[0 for col in range(int(j))] for row in range(int(i))])

	return map

def show_map(map,pose,square_size):
	map_y,map_x = map.shape	
	plt.ion()

	map_tmp = make_map(square_size)
	
	for i in range(map_x):
		for j in range(map_y):
			map_tmp[j,i] = 1024*(1.-(1./(1.+float(np.exp(map[j,i])))))
			print map_tmp[j,i]

	#plt.figure(2)
	cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap',
                                           ['white','black'],
                                           1024)
	plt.imshow(map_tmp,interpolation='nearest',
                    cmap = cmap,origin='lower')
	pos_x,pos_y=xy2map(pose[0],pose[1],square_size)
	plt.plot(pos_x+map_x/2,pos_y+map_y/2,'xb')
	plt.pause(0.001)
	#plt.show(block=False)

def pol2cart(rho, phi):
    x = rho*np.cos(phi)
    y = rho*np.sin(phi)
    return(x, y)


def xy2map(x,y,square_size):
	return int(x/square_size),int(y/square_size)

def inverse_sensor_model(hit):
	if hit == True:
		return np.log10(0.9/0.1)
	else:
		return np.log10(0.4/0.6)

def update_map(map,pose,theta,scan,square_size):
	map_x,map_y = map.shape	
	
	for k in range(len(theta)):
		#print theta[k],scan[k]
		x,y = pol2cart(scan[k],theta[k]+pose[2]*np.pi/180)
		i,j = xy2map(x,y,square_size)
		#print i,j

		pos_x,pos_y=xy2map(pose[0],pose[1],square_size)
		#print i,j
		#print pose[0],pose[1]
		map[pos_y+map_x/2-i,pos_x+map_y/2+j] += 1.1*inverse_sensor_model(True)
	

	return map

def show_laser(theta,scan):

	x,y = pol2cart(scan,theta)

	plt.figure(1)
	plt.plot(x,y)
	#plt.show()


if __name__ == "__main__":

	signal.signal(signal.SIGINT, signal_handler)
	interrupted = False

	robot=RosAriaDriver('/PIONIER2')
	square_size = 0.05 #meters
	map = make_map(square_size)

	while True:
		x = np.arange(0,512)
		theta = ((np.pi/512 )*x)-np.pi/2  # angle in rad 
		pose = robot.GetPose()
		scan=robot.ReadLaser()
		theta, scan = filtr_scan(scan)
		map = update_map(map,pose,theta,scan,square_size)
		show_map(map,pose,square_size)
		
		if interrupted:
			break

#	name = ['map_boxes_0']
#
#	for j in range(len(name)):
#		json_data = open(name[j]+'.json')
#		data = json.load(json_data)
#
#
#		square_size = 0.1 #meters
#
#		map = make_map(square_size)
#		for i in range(len(data)):
#		#for i in range(1):
#			pose = data[i]['pose']
#			time1 = data[i]['time']
#			scan = data[i]['scan']
#
#
#			print pose
#			print time1
#			print 
#			theta, scan = filtr_scan(scan)
#			#print_scan(name[0],theta,scan)
#			#show_laser(theta,scan)
#			map = update_map(map,pose,theta,scan,square_size)
#			show_map(map,pose)
#			#time.sleep(1.0)
#			#print_scan(name[j],theta,scan)	
#			raw_input("Enter ")
