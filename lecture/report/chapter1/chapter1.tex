\section{Introduction}
Boss (fig. \ref{fig:boss}) is an autonomous vehicle that won DARPA Urban Challenge in 2007. It is Chevrolet Tahoe modified for autonomous driving. Boss maintains normal human driving controls (steering wheel, brake and gas
pedals) so that a safety driver can quickly and easily take control during testing. The car has two independent power busses: 12VDC battery and an auxiliary 24VDC providing power for the autonomy hardware. For computation, Boss uses a CompactPCI chassis with ten 2.16GHz
Core2Duo processors, each with 2GB of memory and a pair of gigabit Ethernet
ports. Each computer boots off of a 4GB flash drive, reducing the likelihood of a
disk failure. Two of the machines also mount 500GB hard drives for data logging.
Each computer is also time-synchronized through a custom pulse-per-second
adaptor board.
More detailed information about the vehicle you can find in \cite{DARPA}


\begin{figure}[h]
\centering
\includegraphics[width = 0.7\textwidth]{chapter1/figures/cmu_darpa_boss3.jpg}
\caption{''Boss''}
\label{fig:boss}
\end{figure}


\setcounter{figure}{0}    
\setcounter{table}{0}
\section{Architecture of the control system}
%2. What is the architecture of the control systems? What modules are used and how they are connected?
Architecture of the control system is decomposed into four components (Figure \ref{fig:architecture}). The \textbf{Perception} component processes data from Boss’ sensors to provide key environmental information, including: Vehicle State (globally-referenced position, attitude and speed for Boss), Road World Model (globally-referenced geometric information about the roads parking zones, and intersections in the world), Moving Obstacle Set (an estimation of other vehicles in the vicinity of Boss), Static Obstacle Map (a 2D grid representation of free, dangerous, and lethal
space in the world) and Road Blockages (an estimation of clearly impassable road sections).

The \textbf{Mission Planning} component computes the fastest route through the road
network to reach the next checkpoint in the mission, based on knowledge of road
blockages, speed limits, and the nominal time required to make special maneuvers
such as lane changes or u-turns.
\begin{figure}[h]
\centering
\includegraphics[width = 0.7\textwidth]{chapter1/figures/architecture.png}
\caption{System architecture}
\label{fig:architecture}
\end{figure}

The \textbf{Behavioral Executive} combines the strategic global information provided
by Mission Planning with local traffic and obstacle information provided by Perception and generates a sequence of local tasks for the Motion Planner. It is responsible
for the system’s adherence to various rules of the road, especially those concerning
structured interactions with other traffic and road blockages, and for detection of and
recovery from anomalous situations. 

The \textbf{Motion Planning} component takes the motion goal from the Behavioral Executive and generates and executes a trajectory that will safely drive Boss towards
this goal. Two broad contexts for motion planning exist: on-road driving and unstructured driving.

%7. How the control system logic is organized? What tools are used?
The software infrastructure is a tool box that provides the basic tools required to
build a robotic platform and has the form of common libraries
that provide fundamental capability, such as inter-process communication,
common data types, robotic math routines, data log/playback, and much more.
The following is a list of the tools provided by the infrastructure:
\begin{itemize}
\item Communications Library -- provides tools for serialization, marshaling, UNIX Domain Sockets, TCP/IP, UDP and support publish/subscribe approach.
\item Interfaces Library -- set of interfaces between two processes in the system. It is possible to dynamically load the
interfaces required at run-time.
\item Configuration Library -- provides a parser form Ruby configuration files in order to configure various aspects of the system at run-time.
\item Task Library -- provides an event loop that is triggered at specified frequencies.
\item Debug Logger -- Provides a mechanism for applications to send debug messages
of varying priority to the console, operator control station, log file, etc.
\item Log/Playback -- the data log utility providing a possibility to analyze data.
\item Tartan Racing Operator Control Station (TROCS) -- A graphical interface based
on QT (fig. \ref{fig:trocs}) that provides an operator, engineer, or tester a convenient tool for starting
and stopping the software, viewing status/health information, and debugging the
various tasks that are executing.
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width = 0.9\textwidth]{chapter1/figures/TROCS.png}
\caption{A graphical interface}
\label{fig:trocs}
\end{figure}


\setcounter{figure}{0}    
\setcounter{table}{0}
\section{Sensors}
%1. What sensors are used on the robot, what each of them is used for?
Boss uses a combination of sensors to provide accurate navigation in an urban environment. The sensors and its characteristics are listed in table \ref{tab:sensors}. The
configuration of sensors on Boss is illustrated in figure \ref{fig:sensors}. APLX sensor was used to determinate the position and orientation of the vehicle. Laser scanners (LMS, HDL,  ISF, XT, ARS) and cameras (PGF) helps to estimate position and velocity of static and dynamic obstacles. In total there are more or less 19 different sensors.

\begin{table}[h]
\caption{A description of the sensors incorporated into Boss}
\label{tab:sensors}
\centering
\begin{tabular}{|p{6.1cm}|p{8.1cm}|}\hline
Sensor & Characteristics \\ \hline
Applanix POS-LV 220/420 GPS / IMU (APLX) &
\begin{varwidth}[t]{\linewidth}\begin{itemize}[nosep,after=\strut]
\item sub-meter accuracy with Omnistar
VBS corrections
\item tightly coupled inertial/GPS bridges
GPS-outages
\end{itemize}\end{varwidth}\\  \hline
SICK LMS 291-S05/S14 LIDAR (LMS)&
\begin{varwidth}[t]{\linewidth}\begin{itemize}[nosep,after=\strut]\item 180$^\circ$ / 90$^\circ$ x 0.9$^\circ$ FOV with 1$^\circ$ / 0.5$^\circ$ angular resolution
\item 80m maximum range
\end{itemize}\end{varwidth} \\ \hline
Velodyne HDL-64 LIDAR (HDL)& 
\begin{varwidth}[t]{\linewidth}\begin{itemize}[nosep,after=\strut]\item 360$^\circ$ x 26$^\circ$ FOV with 0.1$^\circ$ angular resolution
\item 70m maximum range
\end{itemize}\end{varwidth}\\ \hline
Continental ISF 172 LIDAR (ISF)& 
\begin{varwidth}[t]{\linewidth}\begin{itemize}[nosep,after=\strut]\item 12$^\circ$ x 3.2$^\circ$ FOV
\item 150m maximum range
\end{itemize}\end{varwidth}\\ \hline
IBEO Alasca XT LIDAR (XT)& 
\begin{varwidth}[t]{\linewidth}\begin{itemize}[nosep,after=\strut]\item 240$^\circ$ x 3.2$^\circ$ FOV
\item 300m maximum range
\end{itemize}\end{varwidth}\\ \hline
Continental ARS 300 Radar (ARS)& 
\begin{varwidth}[t]{\linewidth}\begin{itemize}[nosep,after=\strut]\item 60$^\circ$ / 17$^\circ$ x 3.2$^\circ$ FOV
\item 60m / 200m maximum range
\end{itemize}\end{varwidth} \\ \hline
Point Grey Firefly (PGF)& 
\begin{varwidth}[t]{\linewidth}\begin{itemize}[nosep,after=\strut]\item High dynamic range camera
\item  45$^\circ$ FOV
\end{itemize}\end{varwidth}\\ \hline
\end{tabular}
\end{table}

\begin{figure}[h]
\centering
\includegraphics[width = 0.9\textwidth]{chapter1/figures/sensors.png}
\caption{The mounting location of sensors on the vehicle, refer to Table \ref{tab:sensors} for abbreviations
used in this figure.}
\label{fig:sensors}
\end{figure}


%\begin{figure}[h]
%\centering
%\includegraphics[width = 0.9\textwidth]{chapter1/figures/Boss_sensors.png}
%\caption{Presentation of sensors}
%\label{fig:sensors1}
%\end{figure}

\setcounter{figure}{0}    
\setcounter{table}{0}
\section{Maps}
%3. How are maps created? What are major problems of mapping and how they are dealt with here?
The static obstacle mapping system combines data from the numerous scanning
lasers on the vehicle to generate both instantaneous and temporally filtered
obstacle maps.The instantaneous obstacle map is used in the validation of moving
obstacle hypotheses. The temporally filtered maps are processed to remove
moving obstacles and are filtered to reduce the number of spurious obstacles
appearing in the maps. They use several algorithms but only curb detection and mapping was presented.

Geometric features (curbs, berms and bushes) provide one source of information
for determining road shape in urban and off-road environments. Data from lidar is sufficient to generate accurate, long-range detection of features. Algorithms should be robust to
the variation in features found across the many variants of curbs, berms, ditches,
embankments, etc. The curb detection algorithm presented here exploits the Haar
wavelet to deal with this variety and consists of three main steps: preprocessing, wavelet-based feature extraction, and post-processing.
The preprocessing format the data for feature extraction, next the Wavelet-based feature extraction step analyzes height data through a
discrete wavelet transform using the Haar wavelet. Post-processing applies a few extra heuristics to eliminate false positives and
detect some additional non-road points. Figure  \ref{fig:mapping} illustrates the performance of the
algorithm in a typical on road scene from the Urban Challenge.

\begin{figure}[h]
\centering
\includegraphics[width = 0.9\textwidth]{chapter1/figures/mapping.png}
\caption{Overhead view of a road section from the Final Event course (a). Red points show non-road points (b). Overlay of non-road points on imagery (c).}
\label{fig:mapping}
\end{figure}


\setcounter{figure}{0}    
\setcounter{table}{0}
\section{Localization}
%4. How is robot location determined? What is its original precision and how it is improved?
Boss is capable of either estimating road geometry or localizing itself relative to
roads with known geometry. Given that the shape and location of
paved roads changes infrequently, the approach was to localize relative to paved
roads and estimate the shape of dirt roads, which change geometry more
frequently. This approach has two advantages: eliminate the necessity of estimating road shape in most cases and enables the road shape estimation problem to emphasize geometric cues, which are easier to detect. There are two independent algorithms, one to provide a smooth pose relative to a road network, and second to estimate the shape of dirt roads. The POS-LV is a sensor that estimate a position fusing GPS, inertial and wheel encoder with accuracy 0.1$m$.

%\begin{table}
%\caption{•}
%\centering
%\begin{tabular}{|c|p{3cm}|p{3cm}|} \cline{2-3}
%\multicolumn{1}{c|}{}& Error with GPS and differential corrections & Error after 1km of travel without GPS \\ \hline
%Planar Position (m) & & \\ \hline
%Heading ($^\circ$) & & \\ \hline
%\end{tabular}
%\end{table}


\setcounter{figure}{0}    
\setcounter{table}{0}
\section{Motion Planning}
%5. What algorithm is used in path planning?
The motion planning layer is responsible for executing the current motion goal
issued from the behaviors layer. In all cases, the motion planner creates a path towards the desired goal, then
tracks this path by generating a set of candidate trajectories that follow the path to
varying degrees and selecting from this set the best trajectory according to an
evaluation function. To generate the trajectory they used a model-predictive trajectory generator. This algorithm solve the problem of generating a set of parametrized controls $u(p,x)$ that satisfy state constraints $C(x)$ of a differential equation  \eqref{eq:diff_eqn}.
\begin{equation}
\dot{x} = f(x,u(p,x))
\label{eq:diff_eqn}
\end{equation}
The vehicle model used for Boss combines a curvature limit (the minimum
turning radius), a curvature rate limit (a function of the maximum speed at which
the steering wheel can be turned), maximum acceleration and deceleration, and a
model of the control input latency.

The control inputs are described by two parameterized functions: a time-based
linear velocity function ($v_{cmd} $) and an arc-length-based curvature function ($\kappa_{cmd}$ ):
\begin{equation}
u(p,x) = \left[ v_{cmd}(p,t) + \kappa_{cmd}(p,s) \right]^T
\end{equation}
The linear velocity profile takes the form of a constant profile, linear profile,
linear ramp profile, or a trapezoidal profile (Figure \ref{fig:vel_profile}).

\begin{figure}[h]
\centering
\includegraphics[width = 0.9\textwidth]{chapter1/figures/vel_profiles.png}
\caption{Velocity profiles used by the trajectory generator.}
\label{fig:vel_profile}
\end{figure}

\setcounter{figure}{0}    
\setcounter{table}{0}
\section{Navigation}
%6. What types of navigation are used?
The are two types of navigation: on-road and zone. During on-road navigation, the motion goal from the behavioral system is a
location within a road lane. The motion planner then attempts to generate a
trajectory that moves the vehicle towards this goal location in the desired lane. For each goal, two
trajectories are generated: a smooth trajectory and a sharp trajectory. The smooth
trajectory has the initial curvature parameter fixed to the curvature of the
forwards-predicted vehicle state. The sharp trajectory has the initial curvature
parameter set to an offset value from the forwards-predicted vehicle state to
produce a sharp initial action.

\begin{figure}[h]
\centering
\includegraphics[width=0.45\textwidth]{chapter1/figures/trajec.png}
\includegraphics[width=0.45\textwidth]{chapter1/figures/trajec1.png}
\caption{A single timeframe following a road lane from the DARPA Urban Challenge. The trajectories generated to track
this path (left), and the evaluation of one of these trajectories against both static and dynamic
obstacles (right)}
\end{figure}

During zone navigation, the motion goal from behaviors is a pose within a zone
(such as a parking spot). The motion planner attempts to generate a trajectory that
moves the vehicle towards this goal pose. To efficiently plan a smooth path to a distant goal pose in a zone, they used a
lattice planner that searches over vehicle position ($x, y$), orientation ($\theta$), and speed
($v$). To efficiently generate complex plans over large, obstacle-laden environments,
the planner relies on an anytime, replanning search algorithm known as Anytime
D*.


\begin{figure}[h]
\centering
\includegraphics[width=0.45\textwidth]{chapter1/figures/lattice.png}
\includegraphics[width=0.45\textwidth]{chapter1/figures/lattice1.png}
\caption{Lattice planner}
\end{figure}

\setcounter{figure}{0}    
\setcounter{table}{0}
\section{Conclusion}
%8. What were the main conclusions from the competition?
\begin{itemize}
\item Available sensors are insufficient for urban driving -- The
Velodyne sensor used on Boss  comes close,
but has insufficient angular resolution at long ranges and is unwieldy for
commercial automotive applications.
\item Road shape estimation may be replaced by estimating position relative to the
road -- In urban environments, the shape of roads changes infrequently, and there may
be local anomalies (e.g. a stopped car or construction), but a prior
model of road shape can be used for on-road navigation.
\item Human-level urban driving will require a rich representation -- Boss has a very primitive notion
of what is and is not a vehicle This can cause unwanted behavior; for example, Boss will wait equally
long behind a stopped car (appearing reasonable) and a barrel (appearing
unreasonable), while trying to differentiate between them. A richer representation
including more semantic information will enable future autonomous vehicles to
behave more intelligently.
\end{itemize}